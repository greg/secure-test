# SAST (Static Application Security Testing) Demo Project

## What is this?

A project for testing of GitLab [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) functionality.

This project has a small amount of code for several [supported languages and frameworks](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) to trigger a variety of SAST scanners and output detected "vulnerabilities".

This project can be used to demo SAST functionality and expected results, or it can act as the sand in a sandbox for testing SAST and CI job modifications.

## Usage

1. Import this project to your SaaS namespace or self-managed instance.
1. Trigger a pipeline.
1. :tada:

## Contributing

This project is licensed under MIT license and is accepting contributions.

If you have a proposed improvement, create an issue. Or better yet - make the improvement yourself and submit a merge request!

